﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdgeJs;
using Newtonsoft.Json;

namespace JSParser
{
    public class JSParser
    {
        public JSParser()
        {
            
        }
        public string Parse(string fileList, long firstSensorNumber, int level, string labPath, string xmlPath, string sourcePrefix)
        {
            var func = Edge.Func(@"
                var Parser = require('jsparser');
                return function (input,cb) {
                    try {
                        var p = new Parser();
                        p.run(JSON.parse(input)).then(function(result) { cb(null,result);});
                    } catch (e) {
                        cb(e);
                    }
                }
            ");
            var list = new List<Object>() {fileList, firstSensorNumber, level, labPath, xmlPath, sourcePrefix};
            return func(JsonConvert.SerializeObject(list)).Result.ToString();
        }
    }
}
