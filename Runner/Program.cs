﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var parser = new JSParser.JSParser();
            Environment.SetEnvironmentVariable("EDGE_NODE_PARAMS", "--max_old_space_size=4096");
            Console.WriteLine(parser.Parse("e:/Work/JS0324/list3.txt", 22501, 3, "e:/Work/JS0324/result2/sensored/", "e:/Work/JS0324/result2/xml/", "e:/Work/JS0324/"));
            Console.ReadKey();
        }
    }
}
